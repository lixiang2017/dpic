s%<EOF>%end of file%
s%<EMPTY>%blank%
s%<ERROR>%syntax error%
s%<float>%number%
s%<name>%identifier%
s%<Label>%label%
s%<LaTeX>%LaTeX%
s%<string>%string%
s%<arg>%$ argument%
s%<START>%.PS%
s%<END>%end of file or .PE%
s%<END>%end of file or .PE%
s%";"%"end of line or ;"%
s%<endfor>%end of for {...} contents%
s%<corner>%compass corner: .n .ne .center .end etc%
s%<compare>%logical operator%
s%<param>%attribute .ht .wid etc%
s%<func1>%function (1 arg)%
s%<func2>%function (2 args)%
s%<linetype>%line type: dotted etc%
s%<colrspec>%color spec: colored outlined shaded%
s%<textpos>%text position: ljust rjust above below center%
s%<arrowhd>%arrowhead parameter: <- -> <->%
s%<directon>%direction: up down right left%
s%<primitiv>%drawn object: box circle etc%
s%<envvar>%environmental variable%
